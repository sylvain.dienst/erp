package fr.epsi.edensia.dao.api;

import fr.epsi.edensia.domaine.IngredientPOJO;

public interface IIngredientDAO extends IGenericDAO<IngredientPOJO, Integer> {

}
