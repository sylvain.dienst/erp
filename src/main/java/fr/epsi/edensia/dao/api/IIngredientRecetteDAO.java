package fr.epsi.edensia.dao.api;

import java.util.List;

import fr.epsi.edensia.domaine.IngredientRecettePOJO;

public interface IIngredientRecetteDAO extends IGenericDAO<IngredientRecettePOJO, Integer> {

	List<IngredientRecettePOJO> findlisteIngredientsByRecette(Integer idRecette);

}
