package fr.epsi.edensia.domaine;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "recette")
public class RecettePOJO extends AbstractObjetCreaMAJ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String libelle;
	private Integer nbPersonne;

	private List<IngredientRecettePOJO> listeIngredients;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_recette")
	public Integer getId() {
		return super.getId();
	}

	@Column(name = "libelle")
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Column(name = "nbPersonne")
	public Integer getNbPersonne() {
		return nbPersonne;
	}

	public void setNbPersonne(Integer nbPersonne) {
		this.nbPersonne = nbPersonne;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "recette")
    @Fetch(value = FetchMode.SUBSELECT)
	public List<IngredientRecettePOJO> getListeIngredients() {
		return listeIngredients;
	}

	public void setListeIngredients(List<IngredientRecettePOJO> listeIngredients) {
		this.listeIngredients = listeIngredients;
	}

}
